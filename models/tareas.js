const Tarea = require('./tarea');

class Tareas {
  _listado = {};

  get listadoArr() {
    const listado = [];

    Object.keys(this._listado).forEach((id) => {
      listado.push(this._listado[id]);
    });

    return listado;
  }

  constructor() {
    this._listado = {};
  }

  crearTarea(desc = '') {
    const tarea = new Tarea(desc);
    this._listado[tarea.id] = tarea;
  }

  borrarTarea(id) {
    !!id && delete this._listado[id];
  }

  cargarTareasFormArray(tareas = []) {
    tareas.forEach((tarea) => {
      this._listado[tarea.id] = tarea;
    });
  }

  listadoCompleto() {
    this.listadoArr.forEach((tarea, i) => {
      console.log(
        `${(i + 1).toString().green}. ${tarea.desc} :: ${
          !!tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red
        }`
      );
    });
  }

  listarPendientesCompletadas(completadas = true) {
    this.listadoArr
      .filter((tarea) => !!tarea.completadoEn === completadas)
      .forEach((tarea, i) => {
        console.log(
          `${(i + 1).toString().green}. ${tarea.desc} :: ${
            !!tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red
          }`
        );
      });
  }

  toggleCompletadas(ids = []) {
    ids.forEach((id) => {
      const tarea = this._listado[id];
      !tarea.completadoEn && (tarea.completadoEn = new Date().toISOString());
    });
    this.listadoArr.forEach(({ id }) => {
      if (!ids.includes(id)) {
        this._listado[id].completadoEn = null;
      }
    });
  }
}

module.exports = Tareas;

# Console TODO

Console project to create, read, update and delete to-do.

### Prerequisites 📋

- [Nodejs v15.3.0](https://nodejs.org/en/) - JavaScript runtime environment
- [NPM 7.0.14](https://nodejs.org/en/) - Dependency manager

### Installation 🔧

```
npm i
```

## Versioning 📌

This project uses [SemVer](http://semver.org/) for versioning.

## Author ✒️

- **Jhonatan Tupayachi Hurtado** - _Desarrollo_ - [Web](https://jhonhurachi.com)

## License 📄

This project is licensed under the MIT license
